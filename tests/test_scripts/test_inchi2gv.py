"""unit test for group decomposition."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import sys
import warnings
from os.path import dirname, join, pardir

import pytest


warnings.filterwarnings("ignore", category=DeprecationWarning)

try:
    import openbabel

    assert openbabel
    # A bit of a dirty hack.
    sys.path.insert(0, join(dirname(__file__), pardir, pardir, "scripts"))

    from support.group_decompose import GroupDecomposer, GroupDecompositionError
    from support.groups import GroupVector, GroupsData
    from support.molecule import Molecule

    from equilibrator_cache import Reaction

    from component_contribution import Compound, ccache

except ImportError:
    # TODO: also verify that the OpenBabel version is the latest one
    pytest.skip("Cannot test without OpenBabel.", allow_module_level=True)


@pytest.fixture(scope="module")
def groups_data() -> GroupsData:
    """Get a GroupsData object."""
    return GroupDecomposer().groups_data


@pytest.fixture(scope="module")
def decomposer() -> GroupDecomposer:
    """Get a GroupDecomposer object."""
    return GroupDecomposer()


# TODO: Should this be a utility function in the package?
def decompose_inchi(inchi: str, decomposer: GroupDecomposer) -> GroupVector:
    """Decompose an InChI input using the GroupDecomposer."""
    try:
        mol = Molecule.FromInChI(inchi)
        gr = decomposer.Decompose(mol, raise_exception=True)
        return gr.AsVector().as_dict()
    except GroupDecompositionError:
        return None


def decompose_cid(accession, decomposer):
    """Decompose a compound using its accession in the equilibrator-cache."""
    cpd = ccache.get_compound(accession)
    assert isinstance(cpd, Compound)
    group_dict = decompose_smiles(cpd.smiles, decomposer)
    del cpd
    return group_dict


def decompose_smiles(smiles, decomposer):
    """Decompose a compound given by SMILES string."""
    try:
        mol = Molecule.FromSmiles(smiles)
        gr = decomposer.Decompose(mol, raise_exception=True)
        return gr.AsVector().as_dict()
    except GroupDecompositionError:
        return None


def test_atp_major_ms(decomposer):
    """Test the decomposition of ATP."""
    atp_inchi = (
        "InChI=1S/C10H16N5O13P3/c11-8-5-9(13-2-12-8)15(3-14-5)10-7("
        "17)6(16)4(26-10)1-25-30(21,22)28-31(23,24)27-29(18,"
        "19)20/h2-4,6-7,10,16-17H,1H2,(H,21,22)(H,23,24)(H2,11,12,"
        "13)(H2,18,19,20)/p-4/t4-,6-,7-,10-/m1/s1"
    )
    atp_smiles_neutral = (
        "Nc1c2ncn([C@@H]3O[C@H](COP(=O)([O-])OP(=O)(["
        "O-])OP(=O)([O-])[O-])[C@@H](O)[C@H]3O)c2ncn1"
    )

    assert decompose_inchi(atp_inchi, decomposer) == decompose_smiles(
        atp_smiles_neutral, decomposer
    )

    atp_cpd = ccache.get_compound("KEGG:C00002")
    assert isinstance(atp_cpd, Compound)
    assert atp_cpd.inchi == atp_inchi


@pytest.mark.parametrize(
    "accession, compound_smiles, exp_groups",
    [
        # ATP
        (
            "KEGG:C00002",
            "NC1=C2N=CN([C@@H]3O[C@H](COP([O-])(=O)OP([O-])(=O)OP(O)([O-])=O)"
            "[C@@H](O)[C@H]3O)C2=NC=N1",
            {
                "-OPO3 [H1 Z-1 Mg0]": 1,
                "-OPO2-OPO2- [H0 Z-2 Mg0]": 1,
                "ring =n< [H0 Z0 Mg0]": 1,
                "ring -n= [H0 Z0 Mg0]": 3,
                "ring >c-N [H2 Z0 Mg0]": 1,
                "ring >C-O [H2 Z0 Mg0]": 2,
                "ring -O- [H0 Z0 Mg0]": 1,
                "2-ring =c< [H0 Z0 Mg0]": 2,
                "ring =c- [H1 Z0 Mg0]": 2,
                "ring =c< [H0 Z0 Mg0]": 1,
                "ring -C< [H1 Z0 Mg0]": 2,
                "-C- [H2 Z0 Mg0]": 1,
                "Origin [H0 Z0 Mg0]": 1,
            },
        ),
        # Phosphate
        ("KEGG:C00009", "OP(=O)(O)O", None),
        # Acetate
        (
            "KEGG:C00033",
            "CC(=O)[O-]",
            {
                "-C [H3 Z0 Mg0]": 1,
                "-COO [H0 Z-1 Mg0]": 1,
                "Origin [H0 Z0 Mg0]": 1,
            },
        ),
        # 6-endo-Hydroxycineole - this test does not work on older versions of
        #                           OpenBabel...
        (
            "KEGG:C03092",
            "CC1(C)OC2(C)CCC1C[C@@H]2O",
            {
                "-C [H3 Z0 Mg0]": 3,
                "2-ring -C< [H1 Z0 Mg0]": 1,
                "2-ring >C< [H0 Z0 Mg0]": 2,
                "ring -C- [H2 Z0 Mg0]": 3,
                "ring -O- [H0 Z0 Mg0]": 1,
                "ring >C-O [H2 Z0 Mg0]": 1,
                "Origin [H0 Z0 Mg0]": 1,
            },
        ),
    ],
)
def test_decomposition(accession, compound_smiles, exp_groups, decomposer):
    """Test the decomposition of a compound into groups."""
    groups_res = decompose_cid(accession, decomposer)
    assert groups_res == exp_groups
    groups_res = decompose_smiles(compound_smiles, decomposer)
    assert groups_res == exp_groups


@pytest.mark.parametrize(
    "formula, exp_groups",
    [
        # FMN = FMNH2
        (
            "metanetx.chemical:MNXM119 = metanetx.chemical:MNXM208",
            {"ring -n= [H0 Z0 Mg0]": 1, "ring -n= [H0 Z-1 Mg0]": -1},
        ),
        # FAD = FADH2
        (
            "metanetx.chemical:MNXM33 = metanetx.chemical:MNXM38",
            {"ring -n= [H0 Z0 Mg0]": 1, "ring -n= [H0 Z-1 Mg0]": -1},
        ),
        # ATP + ADP = AMP + A-Tetra-P
        (
            "metanetx.chemical:MNXM3 + metanetx.chemical:MNXM7 = "
            "metanetx.chemical:MNXM14 + metanetx.chemical:MNXM1906",
            {"-OPO3 [H0 Z-2 Mg0]": 1, "-OPO3 [H1 Z-1 Mg0]": -1},
        ),
    ],
)
def test_reaction_decomposition(formula, exp_groups, decomposer, groups_data):
    """Test the decomposition of a few important reactions."""
    rxn = Reaction.parse_formula(ccache.get_compound, formula)
    total_gv = GroupVector(groups_data)
    for compound, coeff in rxn.items():
        mol = Molecule.FromSmiles(compound.smiles)
        total_gv += decomposer.Decompose(mol).AsVector() * coeff
    assert total_gv.as_dict() == exp_groups
