"""A unit test for OpenBabel."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from typing import List

import pytest


try:
    import openbabel
except ImportError:
    # TODO: also verify that the OpenBabel version is the latest one
    pytest.skip("Cannot test without OpenBabel.", allow_module_level=True)

_obSmarts = openbabel.OBSmartsPattern()


def get_focal_atoms(
    smiles: str, smarts: str, positions: List[int]
) -> List[str]:
    """Get the focal atoms found by the SMARTS search.

    :param smiles: a SMILES string
    :param smarts: a SMARTS query string
    :param positions: the positions
    :return: a
    """
    obConversion = openbabel.OBConversion()
    obConversion.AddOption("w", obConversion.OUTOPTIONS)
    obConversion.SetInFormat("smiles")
    obmol = openbabel.OBMol()
    obConversion.ReadString(obmol, smiles)
    _obSmarts.Init(smarts)
    if _obSmarts.Match(obmol):
        match_list = _obSmarts.GetMapList()
        s = set(
            atoms[positions]
            for atoms in map(lambda m: [(n - 1) for n in m], match_list)
        )
        return sorted(s)
    else:
        return []


def test_smarts():
    """Test that openbabel find the expected patterns using SMARTS."""
    smiles_C09844 = "C[C@@]12CC[C@@H](CC1)C(C)(C)O2"  # KEGG:C09844
    smiles_C03092 = "CC1(C)OC2(C)CCC1C[C@@H]2O"  # KEGG:C03092

    assert get_focal_atoms(smiles_C09844, "*[C;H2;R1]*", 1) == [6, 7]
    assert get_focal_atoms(smiles_C03092, "*[C;H2;R1]*", 1) == [6, 7, 9]
    assert get_focal_atoms(smiles_C09844, "*[C;H0;R1](*)(*)*", 1) == [8]
    assert get_focal_atoms(smiles_C03092, "*[C;H0;R1](*)(*)*", 1) == []
