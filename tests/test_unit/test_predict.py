"""unit test for component-contribution predictions."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest

from component_contribution import (
    Compound,
    GibbsEnergyPredictor,
    Reaction,
    ccache,
)
from equilibrator_cache import Q_
from helpers import approx_unit


@pytest.fixture(scope="module")
def comp_contribution() -> GibbsEnergyPredictor:
    """Create GibbsEnergyPredictor object."""
    return GibbsEnergyPredictor()


@pytest.fixture(scope="module")
def atp_compound() -> Compound:
    """Create ATP Compound."""
    return ccache.get_compound("KEGG:C00002")


@pytest.fixture(scope="module")
def atp_hydrolysis_reaction() -> Reaction:
    """Create ATP hydrolysis Reaction."""
    formula = "KEGG:C00002 + KEGG:C00001 = KEGG:C00008 + KEGG:C00009"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.fixture(scope="module")
def transadenylate_reaction() -> Reaction:
    """Create ATP transadenylate Reaction."""
    formula = "KEGG:C00002 + KEGG:C00020 = 2 KEGG:C00008"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.fixture(scope="module")
def gc_dehydrogenase_reaction() -> Reaction:
    """Create 3,4-Dihydroxyphenylethyleneglycol dehydrogenase reaction.

    This reaction must use group contribution to get the reaction energy.
    """
    formula = "KEGG:C05576 + KEGG:C00003 = KEGG:C05577 + KEGG:C00004"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.fixture(scope="module")
def gc_quinate_dehydrogenase_reaction() -> Reaction:
    """Create another GC only reaction."""
    formula = "KEGG:C16595 + KEGG:C00003 = KEGG:C16596 + KEGG:C00004"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.fixture(scope="module")
def unresolved_reaction() -> Reaction:
    """Create a reaction that cannot be resolved using GC at all."""
    formula = (
        "KEGG:C09844 + KEGG:C00003 + KEGG:C00001 = " "KEGG:C03092 + KEGG:C00004"
    )
    return Reaction.parse_formula(ccache.get_compound, formula)


def test_unresolved_reaction(unresolved_reaction, comp_contribution):
    """Test that the reaction cannot be resolved."""
    _, sigma = comp_contribution.standard_dg(unresolved_reaction)
    assert float(sigma / Q_("kJ/mol")) > 1e4


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, exp_delta_g_zero_prime, exp_sigma",
    [
        list(map(Q_, ["6", "0.25M", "298.15K", "-23.4 kJ/mol", "0.6 kJ/mol"])),
        list(map(Q_, ["7", "0.25M", "298.15K", "-25.8 kJ/mol", "0.6 kJ/mol"])),
        list(map(Q_, ["8", "0.25M", "298.15K", "-30.7 kJ/mol", "0.6 kJ/mol"])),
    ],
)
def test_standard_delta_g_prime_calculation_atp(
    p_h,
    ionic_strength,
    temperature,
    exp_delta_g_zero_prime,
    exp_sigma,
    atp_hydrolysis_reaction,
    comp_contribution,
):
    """Test the standard deltaG' of ATP hydrolysis in different pHs."""
    assert atp_hydrolysis_reaction.is_balanced()

    assert not comp_contribution.is_using_group_contribution(
        atp_hydrolysis_reaction
    )

    standard_dg_prime, sigma = comp_contribution.standard_dg_prime(
        atp_hydrolysis_reaction,
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
    )

    approx_unit(standard_dg_prime, exp_delta_g_zero_prime, abs=0.1)
    approx_unit(sigma * 1.96, exp_sigma, abs=0.1)


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, exp_delta_g_zero_prime, exp_sigma",
    [
        list(map(Q_, ["6", "0.25M", "298.15K", "-4.8 kJ/mol", "0.3 kJ/mol"])),
        list(map(Q_, ["7", "0.25M", "298.15K", "-1.8 kJ/mol", "0.3 kJ/mol"])),
        list(map(Q_, ["8", "0.25M", "298.15K", "-0.9 kJ/mol", "0.3 kJ/mol"])),
    ],
)
def test_standard_delta_g_prime_calculation_transadenylate(
    p_h,
    ionic_strength,
    temperature,
    exp_delta_g_zero_prime,
    exp_sigma,
    transadenylate_reaction,
    comp_contribution,
):
    """Test the standard dG' of transadenylate in different pHs."""
    assert transadenylate_reaction.is_balanced()

    assert not comp_contribution.is_using_group_contribution(
        transadenylate_reaction
    )

    standard_dg_prime, sigma = comp_contribution.standard_dg_prime(
        transadenylate_reaction,
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
    )

    approx_unit(standard_dg_prime, exp_delta_g_zero_prime, abs=0.1)
    approx_unit(sigma * 1.96, exp_sigma, abs=0.1)


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, exp_delta_g_zero_prime, exp_sigma",
    [
        list(map(Q_, ["6", "0.25M", "298.15K", "27.8 kJ/mol", "2.0 kJ/mol"])),
        list(map(Q_, ["7", "0.25M", "298.15K", "20.1 kJ/mol", "2.0 kJ/mol"])),
        list(map(Q_, ["8", "0.25M", "298.15K", "14.2 kJ/mol", "2.0 kJ/mol"])),
    ],
)
def test_standard_delta_g_prime_calculation_group_contribution1(
    p_h,
    ionic_strength,
    temperature,
    exp_delta_g_zero_prime,
    exp_sigma,
    gc_dehydrogenase_reaction,
    comp_contribution,
):
    """Test the standard dG' of GC reaction1 in different pHs."""
    assert gc_dehydrogenase_reaction.is_balanced()

    assert comp_contribution.is_using_group_contribution(
        gc_dehydrogenase_reaction
    )

    standard_dg_prime, sigma = comp_contribution.standard_dg_prime(
        gc_dehydrogenase_reaction,
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
    )

    approx_unit(standard_dg_prime, exp_delta_g_zero_prime, abs=0.1)
    approx_unit(sigma * 1.96, exp_sigma, abs=0.1)


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, exp_delta_g_zero_prime, exp_sigma",
    [
        list(map(Q_, ["6", "0.25M", "298.15K", "-38.2 kJ/mol", "5.8 kJ/mol"])),
        list(map(Q_, ["7", "0.25M", "298.15K", "-44.6 kJ/mol", "5.8 kJ/mol"])),
        list(map(Q_, ["8", "0.25M", "298.15K", "-50.4 kJ/mol", "5.8 kJ/mol"])),
    ],
)
def test_standard_delta_g_prime_calculation_group_contribution2(
    p_h,
    ionic_strength,
    temperature,
    exp_delta_g_zero_prime,
    exp_sigma,
    gc_quinate_dehydrogenase_reaction,
    comp_contribution,
):
    """Test the standard dG' of GC reaction2 in different pHs."""
    assert gc_quinate_dehydrogenase_reaction.is_balanced()

    assert comp_contribution.is_using_group_contribution(
        gc_quinate_dehydrogenase_reaction
    )

    standard_dg_prime, sigma = comp_contribution.standard_dg_prime(
        gc_quinate_dehydrogenase_reaction,
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
    )

    approx_unit(standard_dg_prime, exp_delta_g_zero_prime, abs=0.1)
    approx_unit(sigma * 1.96, exp_sigma, abs=0.1)


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, exp_delta_g_zero_prime, exp_sigma",
    [
        list(
            map(Q_, ["6", "0.25M", "298.15K", "-2367.1 kJ/mol", "3.0 kJ/mol"])
        ),
        list(
            map(Q_, ["7", "0.25M", "298.15K", "-2296.4 kJ/mol", "3.0 kJ/mol"])
        ),
        list(
            map(Q_, ["8", "0.25M", "298.15K", "-2227.5 kJ/mol", "3.0 kJ/mol"])
        ),
    ],
)
def test_standard_dg_formation(
    p_h,
    ionic_strength,
    temperature,
    exp_delta_g_zero_prime,
    exp_sigma,
    atp_compound,
    comp_contribution,
):
    """Test the standard dG' of formation of ATP."""
    standard_dgf, sigma = comp_contribution.standard_dgf_prime(
        atp_compound,
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
    )

    approx_unit(standard_dgf, exp_delta_g_zero_prime, abs=0.1)
    approx_unit(sigma * 1.96, exp_sigma, abs=0.1)
