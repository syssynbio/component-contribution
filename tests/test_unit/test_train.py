"""unit test the training process of component-contribution."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import warnings

import pytest

from component_contribution import (
    ComponentContributionTrainer,
    GibbsEnergyPredictor,
    Reaction,
    ccache,
)
from component_contribution.training_data import ToyTrainingData, TrainingData
from equilibrator_cache import Q_
from helpers import approx_unit


warnings.filterwarnings("ignore", message="numpy.dtype size changed")


@pytest.fixture(scope="module")
def training_data():
    """Create a ToyTrainingData object."""
    return ToyTrainingData()


@pytest.fixture(scope="module")
def comp_contribution(training_data: TrainingData) -> GibbsEnergyPredictor:
    """Create a GibbsEnergyPredictor object."""
    return ComponentContributionTrainer.train(training_data=training_data)


@pytest.fixture(scope="module")
def atp_hydrolysis_reaction() -> Reaction:
    """Create a ATP hydrolysis reaction."""
    formula = "KEGG:C00002 + KEGG:C00001 <=> KEGG:C00008 + KEGG:C00009"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.fixture(scope="module")
def transadenylate_reaction() -> Reaction:
    """Create a ATP transadenylate reaction."""
    formula = "KEGG:C00002 + KEGG:C00020 <=> 2 KEGG:C00008"
    return Reaction.parse_formula(ccache.get_compound, formula)


def test_train(comp_contribution):
    """Test the training process in terms of matrix dimensions."""
    n_groups = 163
    n_compounds = 96
    n_non_decomposable = 6
    n_reactions = 48
    n_groups_extended = n_groups + n_non_decomposable

    assert comp_contribution.Ng == n_groups  # number of real groups
    p = comp_contribution.params

    assert p.train_b.shape == (n_reactions,)
    assert p.train_S.shape == (n_compounds, n_reactions)
    assert p.train_G.shape == (n_compounds, n_groups_extended)
    assert p.train_w.shape == (n_reactions,)
    assert p.inv_GS.shape == (n_reactions, n_groups_extended)
    assert p.dG0_rc.shape == (n_compounds,)
    assert p.dG0_cc.shape == (n_compounds,)
    assert p.dG0_gc.shape == (n_groups_extended,)


def test_standard_dg_calculation_atp(
    comp_contribution, atp_hydrolysis_reaction
):
    """Test the trained CC model on ATP hydrolysis."""
    delta_g_zero, sigma = comp_contribution.standard_dg(atp_hydrolysis_reaction)
    approx_unit(delta_g_zero, Q_("12.6 kJ/mol"), abs=0.1)
    approx_unit(sigma * 1.96, Q_("3.5 kJ/mol"), abs=0.1)


@pytest.mark.parametrize(
    "ph_value, ionic_strength, temperature, exp_standard_dg_prime, exp_sigma",
    [
        list(map(Q_, ["6", "0.25M", "298.15K", "-24.2 kJ/mol", "3.5 kJ/mol"])),
        list(map(Q_, ["7", "0.25M", "298.15K", "-26.6 kJ/mol", "3.5 kJ/mol"])),
        list(map(Q_, ["8", "0.25M", "298.15K", "-31.5 kJ/mol", "3.5 kJ/mol"])),
    ],
)
def test_standard_dg_prime_calculation_atp(
    ph_value,
    ionic_strength,
    temperature,
    exp_standard_dg_prime,
    exp_sigma,
    atp_hydrolysis_reaction,
    comp_contribution,
):
    """Test the trained CC model on ATP hydrolysis in different pH levels."""
    delta_g_zero_prime, sigma = comp_contribution.standard_dg_prime(
        atp_hydrolysis_reaction,
        p_h=ph_value,
        ionic_strength=ionic_strength,
        temperature=temperature,
    )

    approx_unit(delta_g_zero_prime, exp_standard_dg_prime, abs=0.1)
    approx_unit(sigma * 1.96, exp_sigma, abs=0.1)


@pytest.mark.parametrize(
    "ph_value, ionic_strength, temperature, exp_delta_g_zero_prime, exp_sigma",
    [
        list(map(Q_, ["6", "0.25M", "298.15K", "-7.5 kJ/mol", "2.4 kJ/mol"])),
        list(map(Q_, ["7", "0.25M", "298.15K", "-4.4 kJ/mol", "2.4 kJ/mol"])),
        list(map(Q_, ["8", "0.25M", "298.15K", "-3.5 kJ/mol", "2.4 kJ/mol"])),
    ],
)
def test_standard_dg_prime_calculation_transadenylate(
    ph_value,
    ionic_strength,
    temperature,
    exp_delta_g_zero_prime,
    exp_sigma,
    transadenylate_reaction,
    comp_contribution,
):
    """Test the trained CC model on the transadenylate reaction."""
    delta_g_zero_prime, sigma = comp_contribution.standard_dg_prime(
        transadenylate_reaction,
        p_h=ph_value,
        ionic_strength=ionic_strength,
        temperature=temperature,
    )
    approx_unit(delta_g_zero_prime, exp_delta_g_zero_prime, abs=0.1)
    approx_unit(sigma * 1.96, exp_sigma, abs=0.1)
