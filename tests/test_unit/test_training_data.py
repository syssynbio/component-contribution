"""Unit test of the training data in quilt."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest
import quilt

from component_contribution import DEFAULT_QUILT_PKG


@pytest.fixture(scope="module")
def quilt_pkg():
    """Download data from quilt."""
    quilt.install(DEFAULT_QUILT_PKG, force=True)
    return quilt.load(DEFAULT_QUILT_PKG)


def test_read_toy_data(quilt_pkg):
    """Verify that the size of the toy data is correct."""
    df = quilt_pkg.train.toy_training_data()
    assert df.shape == (48, 7)


def test_read_tecrdb(quilt_pkg):
    """Verify that the size of the TECRDB data is correct."""
    df = quilt_pkg.train.TECRDB()
    assert df.shape == (4544, 14)


def test_read_formations(quilt_pkg):
    """Verify that the size of the Formation data is correct."""
    df = quilt_pkg.train.formation_energies_transformed()
    assert df.shape == (227, 10)


def test_read_redox(quilt_pkg):
    """Verify that the size of the Redox data is correct."""
    df = quilt_pkg.train.redox()
    assert df.shape == (13, 13)
