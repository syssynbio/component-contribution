"""Command line script for managing groups in equilibrator-cache."""
# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import logging
from os.path import dirname, join, pardir

import click
import pandas as pd
import quilt
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tqdm import tqdm

import click_log
from component_contribution import (
    DEFAULT_QUILT_PKG,
    ComponentContributionTrainer,
)
from component_contribution.training_data import FullTrainingData
from equilibrator_cache import Compound
from support.group_decompose import GroupDecomposer, GroupDecompositionError
from support.groups import DEFAULT_GROUPS_DATA
from support.molecule import Molecule, OpenBabelError


logger = logging.getLogger()
click_log.basic_config(logger)
Session = sessionmaker()

DEFAULT_LOCATION = join(
    dirname(__file__),
    pardir,
    pardir,
    "equilibrator-cache",
    "src",
    "equilibrator_cache",
    "cache",
    "compounds.sqlite",
)
DATABASE_URL = f"sqlite:///{DEFAULT_LOCATION}"


@click.group()
@click.help_option("--help", "-h")
@click_log.simple_verbosity_option(
    logger,
    default="INFO",
    show_default=True,
    type=click.Choice(["CRITICAL", "ERROR", "WARN", "INFO", "DEBUG"]),
)
def cli():
    """Command line interface to populate and update the equilibrator cache."""
    pass


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--db-url",
    metavar="URL",
    default=DATABASE_URL,
    show_default=True,
    help="A string interpreted as an rfc1738 compatible database URL.",
)
@click.option(
    "--quilt_pkg",
    metavar="PATH",
    default="equilibrator/cache",
    show_default=True,
    help="Quilt package name for the compound cache.",
)
@click.option(
    "--batch-size",
    type=int,
    default=30000,
    show_default=True,
    help="The size of batches of compounds considered at a time.",
)
def decompose(db_url: str, quilt_pkg: str, batch_size: int) -> None:
    """Calculate and store thermodynamic information for compounds."""
    engine = create_engine(db_url)
    session = Session(bind=engine)

    # Query for all compounds.
    query = session.query(Compound.id, Compound.mnx_id, Compound.smiles).filter(
        Compound.smiles.isnot(None), Compound.group_vector.is_(None)
    )

    logger.debug("decomposing all compounds with structures")
    input_df = pd.read_sql_query(query.statement, query.session.bind)

    group_decomposer = GroupDecomposer()

    with tqdm(total=len(input_df), desc="Analyzed") as pbar:
        for index in range(0, len(input_df), batch_size):
            view = input_df.iloc[index : index + batch_size, :]
            compounds = []
            for row in view.itertuples(index=False):
                try:
                    # it is important to use here the SMILES representation
                    # of the molecule, and not the InChI, since it hold the
                    # information about the most abundant species at pH 7.
                    mol = Molecule.FromSmiles(row.smiles)
                    decomposition = group_decomposer.Decompose(
                        mol, ignore_protonations=False, raise_exception=True
                    )
                    group_vector = decomposition.AsVector()
                    compounds.append(
                        {"id": row.id, "group_vector": list(group_vector.flat)}
                    )
                    logger.debug(
                        "Decomposition of %r: %r", row.mnx_id, decomposition
                    )
                except OpenBabelError as e:
                    compounds.append({"id": row.id, "group_vector": list()})
                    logger.debug(str(e))
                except GroupDecompositionError as e:
                    compounds.append({"id": row.id, "group_vector": list()})
                    logger.debug(str(e))

            session.bulk_update_mappings(Compound, compounds)
            session.commit()
            pbar.update(len(view))

    logger.debug("pushing DB changes to quilt (i.e. adding group vectors)")
    quilt.install(quilt_pkg, force=True)

    # rebuild the 'compounds' table, now with the new group vectors
    compounds_df = pd.read_sql_table("compounds", engine)
    quilt.build(join(quilt_pkg, "compounds", "compounds"), compounds_df)

    # build the group names and add them to quilt as well, this list is
    # needed later by the training scripts, because they cannot access the
    # GroupsData class directly (to get rid of their OpenBabel dependence)
    quilt.build(
        join(quilt_pkg, "groups", "definitions"),
        DEFAULT_GROUPS_DATA.ToDataFrame(),
    )

    quilt.push(quilt_pkg, is_public=True)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--quilt_pkg",
    metavar="PATH",
    default=DEFAULT_QUILT_PKG,
    show_default=True,
    help="Quilt package name for the component contribution model.",
)
def train(quilt_pkg: str) -> None:
    """Train the Component Contribution model and push to quilt."""
    train_data = FullTrainingData()
    cc = ComponentContributionTrainer.train(train_data)
    quilt.install(quilt_pkg, force=True)
    for param_name, param_value in cc.params._asdict().items():
        quilt.build(join(quilt_pkg, "parameters", param_name), param_value)
    quilt.push(quilt_pkg, is_public=True)


if __name__ == "__main__":
    cli()
